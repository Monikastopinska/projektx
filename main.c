#include <stdio.h>
#include <stdlib.h>
#include <math.h>

float Mean(float *x, float mean, int size);
void Test(FILE *file, float *x, float *y, float *rho);
void Bbl_sort(float *x, int size);
float SD(float *data, float mean, int size);
int main()
{
    int size=50;
    FILE *file;
    file=fopen("P0001_attr.txt","r+");

    if(file == NULL){
        printf("null");
        exit(1);
    }
    else{
        float mean=0.0;
        float *x;
        x = (float *)calloc(size, sizeof(float));
        float *y ;
        y = (float *)calloc(size, sizeof(float));
        float *rho;
        rho= (float *)calloc(size, sizeof(float));

         Test(file, x, y, rho);

         printf("\n\nDane dla kolumny LP");
         Mean(x, mean, size);
         Bbl_sort(x, size);
         SD(x, mean, size);
         printf("\n\nDane dla kolumny Y");
         Mean(y, mean, size);
         Bbl_sort(y, size);
         SD(y, mean, size);
         printf("\n\nDane dla kolumny RHO");
         Mean(rho, mean, size);
         Bbl_sort(rho, size);
         SD(rho, mean, size);

    }

    fclose(file) ;


    return 0;
}


void Test(FILE *file, float *x, float *y, float *rho){
    fseek(file, 16, SEEK_SET);

    for(int i=0; i<9; i++)
    {
        fscanf(file, "%f", &x[i]);
        //printf("%.5f\t",x[i]);

        fseek(file, 2,  SEEK_CUR);
        fscanf(file,"%f", &y[i]);
        // printf("%.5f\t",y[i]);

        fseek(file, 1,SEEK_CUR);
        fscanf(file, "%f", &rho[i]);
        // printf("%.3f\n",rho[i]);

        fseek(file, 4, SEEK_CUR);
    }

    fseek(file, 1, SEEK_CUR);

    for(int i=9; i<50; i++)
    {
        fscanf(file, "%f", &x[i]);
        printf("%.5f\t", x[i]);

        fseek(file, 2, SEEK_CUR);
        fscanf(file,"%f", &y[i]);
         printf("%.5f\t",y[i]);

        fseek(file, 1,SEEK_CUR);
        fscanf(file, "%f", &rho[i]);
         printf("%.3f\n",rho[i]);

        fseek(file, 5, SEEK_CUR);
    }
    return;
}
float Mean(float *x, float mean, int size){

    for(int i=0 ; i<size; i++)
        mean+=x[i];

    mean=mean/size;
    printf("\nSREDNIA=%f", mean);
    return mean;
}
void Bbl_sort(float *x, int size) {
    float temp=0.0;

    for (int i = 0; i < 50; i++) {
        for (int j = 0; j < 50 - i; j++) {
            if (x[j - 1] > x[j]) {
                temp=x[j] ;
                x[j] = x[j - 1];
                x[j - 1] = temp;
            }
        }
    }
    if(size%2==0){
        int half= size/2;
    float median= (x[half]+x[half-1])/2;
    printf("\nMEDIANA = %f", median);}
    else{
        printf("\nMEDIANA = %f", x[size/2]);
    }
}
float SD(float *data, float mean, int size) {
    float SD = 0.0;
    int i;
    for (i = 0; i < size; ++i)
        SD += pow(data[i] - mean, 2);

        SD=sqrt(SD / size);
    printf("\nODCHYLENIE STANDARDOWE= %f", SD);

    return sqrt(SD / size);
}